let age:number = 23; 

if(age<50)
    age+=10;

 console.log(age); 

let sales:number =223_233_323 //number
let course:string ="Angular"; //string
let is_published=true; //boolean
let level ;   //any

function render(document:any){
    console.log(`The document has been rendered`+document);
}

render("HTML");

 //arrays 
let number:number[]= [4,67,89];
console.log(number[1]);

//Tuples
 let person:[string,number,boolean]=["John",32,true];

 //Enums
  const enum color{red="Red",green="Green",blue="Blue"};
 console.log(color.red); 

 //Fuction
 function greet(name: string="gagan"):String {
     return "hello"+name+ "!";
 }

 greet("John");

 //Type Aliases
 type aliasName=string;
 let fullName:aliasName="Gagan Kumar saini";

 type Employee={ 
    id:number,
    name?:String,
    retire:(date:Date)=>void
}
 //objects

 let employee:Employee={
    id:1,
    name:'ram',
    retire:(date: Date) =>{
    console.log('Employee retired on ' + date)}
 };
 console.log(employee.name)
 employee.retire (new Date());

 //Type Assertion
 let something : any ="I am a string"
 if(typeof something === "string"){
     console.log(something.toUpperCase())
 }else{
     console.log(something.toString())
 }

 //union Type 
 let unionType : number | string =5;
 unionType = "Hello World";
 unionType= 56;
 console.log(unionType);

 function kgToLbs(weight: number| string):number{
    if(typeof weight==='string'){
        return parseInt(weight)/2.20462;
    } else {
       return weight *  2.20462;
    }
 }

 //Intersection Types

 type Draggable={
    drag:()=> void 

 }
 type Resizable={
    resize: ()=>void
 }
 let draggableElement:Draggable & Resizable={
    drag:function()
    {console.log("dragging element");}
    ,resize:function()
    {console.log("resizing Element");}}
 draggableElement.drag();
 draggableElement.resize();

 //Literal Type
 type Quantity = 50|100;
 let quantity: Quantity = 100;

 type Metric = 'cm'|'mm';

//Nullable Types
let lengthInCm: number | null = 39;
lengthInCm = null;
if (lengthInCm !=null) {
    console.log(`Length is ${lengthInCm}`);
}else{
    console.log('Length is undefined');
}

//Optional Chaining

type Customer ={
    Birthday?:Date
}
function getCustomer(id:number):Customer | null{
    return id===0 ? null:{Birthday : new Date()}
}

let Customer =getCustomer(0)
//optional property access operator
console.log(Customer?.Birthday?.getFullYear)



